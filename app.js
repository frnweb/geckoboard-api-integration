require('dotenv').config();
const fetch = require('node-fetch');

//pulling information from the .env and storing it for use in the API call. These parameters are required for the Infinity Metrics API.
const inputData = {
  username: process.env.INFINITY_USERNAME,
  password: process.env.INFINITY_PASSWORD,
  igrp: process.env.INFINITY_IGRP,
  tz: 'America/Chicago',
  sort: 'pageCountTotal-desc',
  group: 'ch',
  limit: 20,
  format: 'jsonarray',
  apiKey: process.env.GECKO_API_KEY,
}

//stores the current date used in the getMetricData() API call. These parameters are required for the Infinity Metrics API. 
const getCurrentDate = () => {
  const currentDate = new Date().toISOString();
  const date = currentDate.split('T')[0];
  return `${date.split('-')[0]}-${date.split('-')[1]}-${date.split('-')[2]}`;
}

//stores the date 7 days before the current date. This is used in the getLastWeekMetricData() API call. These parameters are required for the Infinity Metrics API. 
const getWeekAgoDate = () => {
  const ourDate = new Date();
  const pastDate = ourDate.getDate() - 7;
  ourDate.setDate(pastDate);
  const splitDate = ourDate.toISOString().split('T')[0];
  return `${splitDate.split('-')[0]}-${splitDate.split('-')[1]}-${splitDate.split('-')[2]}`;  
}

//stores the date 28 days before the current date. This is used in the getLastMonthMetricData() API call. These parameters are required for the Infinity Metrics API. 
const getMonthAgoDate = () => {
  const ourDate = new Date();
  const pastDate = ourDate.getDate() - 28;
  ourDate.setDate(pastDate);
  const splitDate = ourDate.toISOString().split('T')[0];
  return `${splitDate.split('-')[0]}-${splitDate.split('-')[1]}-${splitDate.split('-')[2]}`; 
}



// Grabs the parameters from inputData, getCurrentDate(), and returns the API response as a json. This is passed to formatData().
const getMetricData = async () => {
  const { igrp, sort, group, limit, format, tz } = inputData;

  // Start and end dates can be overriden with env vars. Defaults to current date.
  const startDate = process.env.START_DATE || getCurrentDate();
  const endDate = process.env.END_DATE || getCurrentDate();

  let headers = new fetch.Headers();
  headers.set('Authorization', 'Basic ' + Buffer.from(inputData.username + ":" + inputData.password).toString('base64'));

  const params = `?igrp=${igrp}&startDate=${startDate}&endDate=${endDate}&tz=${tz}&sort[]=${sort}&group[]=${group}&limit=${limit}&format=${format}&customMetrics[goals15254]=goalCount_goal_15254&customMetrics[goals15257]=goalCount_goal_15257&customMetrics[goals15251]=goalCount_goal_15251&customMetrics[goal11909]=goalCount_goal_11909`;
  const res = await fetch(`https://api.infinitycloud.com/reports/v2/metrics/channels${params}`, {
    headers: headers,
  });

  return await res.json();
}

// Grabs the parameters from inputData, getWeekAgoDate(), and returns the API response as a json. This is passed to formatLastWeekData().
const getLastWeekMetricData = async () => {
  const { igrp, sort, group, limit, format, tz } = inputData;

  // Start and end dates are current date - 1 week.
  const startDate = getWeekAgoDate();
  const endDate = getWeekAgoDate();

  let headers = new fetch.Headers();
  headers.set('Authorization', 'Basic ' + Buffer.from(inputData.username + ":" + inputData.password).toString('base64'));

  const params = `?igrp=${igrp}&startDate=${startDate}&endDate=${endDate}&tz=${tz}&sort[]=${sort}&group[]=${group}&limit=${limit}&format=${format}&customMetrics[goals15254]=goalCount_goal_15254&customMetrics[goals15257]=goalCount_goal_15257&customMetrics[goals15251]=goalCount_goal_15251&customMetrics[goal11909]=goalCount_goal_11909`;
  const res = await fetch(`https://api.infinitycloud.com/reports/v2/metrics/channels${params}`, {
    headers: headers,
  });

  return await res.json();
}

// Grabs the parameters from inputData, getMonthAgoDate(), and returns the API response as a json. This is passed to formatMonthAgoData().
const getLastMonthMetricData = async () => {
  const { igrp, sort, group, limit, format, tz } = inputData;

  // Start and end dates are current date - 28 days.
  const startDate = getMonthAgoDate();
  const endDate = getCurrentDate();

  let headers = new fetch.Headers();
  headers.set('Authorization', 'Basic ' + Buffer.from(inputData.username + ":" + inputData.password).toString('base64'));

  const params = `?igrp=${igrp}&startDate=${startDate}&endDate=${endDate}&tz=${tz}&sort[]=${sort}&group[]=${group}&limit=${limit}&format=${format}&customMetrics[goals15254]=goalCount_goal_15254&customMetrics[goals15257]=goalCount_goal_15257&customMetrics[goals15251]=goalCount_goal_15251&customMetrics[goal11909]=goalCount_goal_11909`;
  const res = await fetch(`https://api.infinitycloud.com/reports/v2/metrics/channels${params}`, {
    headers: headers,
  });

  return await res.json();
}

//Similar to above API calls, but instead of channel group from inputData(), it's grabbing the segementGroupId in order to get data per facility.
const getLastMonthFacilityData = async () => {
  const { igrp, sort, group, limit, format, tz } = inputData;

  // Start and end dates are current date - 28 days.
  const startDate = getMonthAgoDate();
  const endDate = getCurrentDate();

  let headers = new fetch.Headers();
  headers.set('Authorization', 'Basic ' + Buffer.from(inputData.username + ":" + inputData.password).toString('base64'));

  const params = `?igrp=${igrp}&startDate=${startDate}&endDate=${endDate}&tz=${tz}&sort[]=${sort}&group[]=segmentGroupId&limit=30&format=${format}&customMetrics[goals15254]=goalCount_goal_15254&customMetrics[goals15257]=goalCount_goal_15257&customMetrics[goals15251]=goalCount_goal_15251&customMetrics[goal11909]=goalCount_goal_11909`;
  const res = await fetch(`https://api.infinitycloud.com/reports/v2/metrics/channels${params}`, {
    headers: headers,
  });

  return await res.json();
}

//Similar to above API calls, but instead of channel group from inputData(), it's grabbing the metricMonth in order to get data per Month.
const getAllMonthsData = async () => {
  const { igrp, sort, group, limit, format, tz } = inputData;

  // Start and end dates can be overriden with env vars. Defaults to current date.
  const startDate = process.env.START_DATE || getCurrentDate();
  const endDate = process.env.END_DATE || getCurrentDate();

  let headers = new fetch.Headers();
  headers.set('Authorization', 'Basic ' + Buffer.from(inputData.username + ":" + inputData.password).toString('base64'));

  const params = `?igrp=${igrp}&startDate=2020-01-01&endDate=${endDate}&tz=${tz}&sort[]=${sort}&group[]=metricMonth&limit=${limit}&format=${format}&customMetrics[goals15254]=goalCount_goal_15254&customMetrics[goals15257]=goalCount_goal_15257&customMetrics[goals15251]=goalCount_goal_15251&customMetrics[goal11909]=goalCount_goal_11909`;
  const res = await fetch(`https://api.infinitycloud.com/reports/v2/metrics/channels${params}`, {
    headers: headers,
  });

  return await res.json();  
}



// Format data returned from Infinity for Geckoboard Call Goals, structure must match the dataset created with datasetPut().
const formatData = (data) => {
  let initData = [];

  let sortData = [];

  let goalData = []

  data.forEach(function(data) {
    if (data.chName == "FRN Emails" || data.chName ==  "Social" || data.chName == "Website Referrer" || data.chName == "Website Referrer (Our Sites)" || data.chName == "Unknown" || data.chName == "Print" || data.chName == "Ads: Other" || data.chName == "Ads: Social" || data.chName == "Discontinued" || data.chName == "Offline" || data.chName == "SMS" || data.chName == "Video") {
      sortData.push({
        allcalls:parseFloat(data.callCountTotal),
        callcount:parseFloat(data.callGoalCountTotal), 
        channel:data.chName, 
        qualified:parseFloat(data.goal11909), 
        greaterthanfour:parseFloat(data.goals15251), 
        greaterthanten:parseFloat(data.goals15254), 
        greaterthanfifteen:parseFloat(data.goals15257), 
        callgoals:parseFloat(data.goalCountTotal),
        qualleads:parseFloat(data.goals15254) +  parseFloat(data.goals15257)  
      });
    } else {
    initData.push(
      {
        allcalls:parseFloat(data.callCountTotal), 
        callcount:parseFloat(data.callGoalCountTotal), 
        channel:data.chName, 
        qualified:parseFloat(data.goal11909), 
        greaterthanfour:parseFloat(data.goals15251), 
        greaterthanten:parseFloat(data.goals15254), 
        greaterthanfifteen:parseFloat(data.goals15257), 
        callgoals:parseFloat(data.goalCountTotal),
        qualleads:parseFloat(data.goals15254) +  parseFloat(data.goals15257) 
      });
    }  
  })

  initData.forEach(function(e) {
    if (e.channel === "Directories") {
      goalData.splice(4,0, e)
    } else if (e.channel === "Organic Search") {
      goalData.splice(3,0, e)
    } else if (e.channel === "Paid Search") {
      goalData.splice(2,0, e)
    } else if (e.channel === "Website Direct") {
      goalData.splice(1,0, e)
    } else if (e.channel === "User-Distributed") {
      goalData.splice(0,0, e)
    }
  })

  var val = sortData.reduce((accumulator, currentValue) => {
    return {
      allcalls: accumulator.allcalls + currentValue.allcalls,
      callcount: accumulator.callcount + currentValue.callcount,
      channel: "Other",
      qualified: accumulator.qualified + currentValue.qualified, 
      greaterthanfour:accumulator.greaterthanfour + currentValue.greaterthanfour,
      greaterthanten:accumulator.greaterthanten + currentValue.greaterthanten,
      greaterthanfifteen:accumulator.greaterthanfifteen + currentValue.greaterthanfifteen, 
      callgoals:accumulator.callgoals + currentValue.callgoals,
      qualleads:accumulator.qualleads + currentValue.qualleads
    }
  });

  goalData.unshift(val);

  return goalData;
};

// Format data returned from Infinity for Geckoboard Call Goals compared to last weeks, it is getting both data object arrays from getCurrentDate() and getWeekAgoDate(), structure must match the dataset created with lastWeekDatasetPut().
const formatLastWeekData = (data, lastWeekData) => {

  let todayData = [];

  let lastData = [];

  data.forEach(function(data) {
    todayData.push(
      {
        callstoday:parseFloat(data.callGoalCountTotal), 
      });
  })

  lastWeekData.forEach(function(lastWeekData) {
    lastData.push(
      {
        callslastweek:parseFloat(lastWeekData.callGoalCountTotal), 

      });
  })

  var todayVal = todayData.reduce((accumulator, currentValue) => {
    return {
      callstoday: accumulator.callstoday + currentValue.callstoday
    }
  });

  var lastWeekVal = lastData.reduce((accumulator, currentValue) => {
    return {
      callslastweek: accumulator.callslastweek + currentValue.callslastweek
    }
  });

  val = Object.values(todayVal)[0];
  valtwo = Object.values(lastWeekVal)[0];

  let initData = [{"x_axis": new Date().toISOString().slice(0,10), callstoday:val, callslastweek:valtwo }];

  return initData;
};

// Format data returned from Infinity for Geckoboard Call Goals from 28 days ago, structure must match the dataset created with lastMonthDatasetPut().
const formatMonthAgoData = (data) => {
  let initData = [];

  let sortData = [];

  let goalData = []

  data.forEach(function(data) {
    if (data.chName == "FRN Emails" || data.chName ==  "Social" || data.chName == "Website Referrer" || data.chName == "Website Referrer (Our Sites)" || data.chName == "Unknown" || data.chName == "Print" || data.chName == "Ads: Other" || data.chName == "Ads: Social" || data.chName == "Discontinued" || data.chName == "Offline" || data.chName == "SMS" || data.chName == "Video") {
      sortData.push({
        allcalls:parseFloat(data.callCountTotal),
        callcount:parseFloat(data.callGoalCountTotal), 
        channel:data.chName, 
        qualified:parseFloat(data.goal11909), 
        greaterthanfour:parseFloat(data.goals15251), 
        greaterthanten:parseFloat(data.goals15254), 
        greaterthanfifteen:parseFloat(data.goals15257), 
        callgoals:parseFloat(data.goalCountTotal),
        qualleads:parseFloat(data.goals15254) +  parseFloat(data.goals15257),
        avgdayqualcalls:parseFloat(data.callGoalCountTotal) / 28  
      });
    } else {
    initData.push(
      {
        allcalls:parseFloat(data.callCountTotal), 
        callcount:parseFloat(data.callGoalCountTotal), 
        channel:data.chName, 
        qualified:parseFloat(data.goal11909), 
        greaterthanfour:parseFloat(data.goals15251), 
        greaterthanten:parseFloat(data.goals15254), 
        greaterthanfifteen:parseFloat(data.goals15257), 
        callgoals:parseFloat(data.goalCountTotal),
        qualleads:parseFloat(data.goals15254) +  parseFloat(data.goals15257),
        avgdayqualcalls:parseFloat(data.callGoalCountTotal) / 28
      });
    }  
  })

  initData.forEach(function(e) {
    if (e.channel === "Directories") {
      goalData.splice(4,0, e)
    } else if (e.channel === "Organic Search") {
      goalData.splice(3,0, e)
    } else if (e.channel === "Paid Search") {
      goalData.splice(2,0, e)
    } else if (e.channel === "Website Direct") {
      goalData.splice(1,0, e)
    } else if (e.channel === "User-Distributed") {
      goalData.splice(0,0, e)
    }
  })

  var val = sortData.reduce((accumulator, currentValue) => {
    return {
      allcalls: accumulator.allcalls + currentValue.allcalls,
      callcount: accumulator.callcount + currentValue.callcount,
      channel: "Other",
      qualified: accumulator.qualified + currentValue.qualified, 
      greaterthanfour:accumulator.greaterthanfour + currentValue.greaterthanfour,
      greaterthanten:accumulator.greaterthanten + currentValue.greaterthanten,
      greaterthanfifteen:accumulator.greaterthanfifteen + currentValue.greaterthanfifteen, 
      callgoals:accumulator.callgoals + currentValue.callgoals,
      qualleads:accumulator.qualleads + currentValue.qualleads,
      avgdayqualcalls:accumulator.avgdayqualcalls + currentValue.avgdayqualcalls
    }
  });

  goalData.unshift(val);

  return goalData;
};

// Format data returned from Infinity for Geckoboard Call Goals from 28 days ago, structure must match the dataset created with lastMonthDatasetPut().
const formatFacilityData = (data) => {

  let blackBearData = [];

  let talbottData = [];

  let skywoodData = [];

  let michaelsData = [];

  let finalData = [];

  data.forEach(function(data) {
   if (data.segmentGroupName == "Black Bear Lodge" || data.segmentGroupName == "IOP: Foundations Atlanta at Midtown" || data.segmentGroupName == "IOP: Foundations Atlanta at Roswell" || data.segmentGroupName == "IOP: Foundations Nashville") {
    blackBearData.push(
      {
        facilityvisits:parseFloat(data.landCountTotal),
        allcalls:parseFloat(data.callCountTotal), 
        callcount:parseFloat(data.callGoalCountTotal), 
        facilityname:data.segmentGroupName, 
        qualified:parseFloat(data.goal11909), 
        greaterthanfour:parseFloat(data.goals15251), 
        greaterthanten:parseFloat(data.goals15254), 
        greaterthanfifteen:parseFloat(data.goals15257), 
        callgoals:parseFloat(data.goalCountTotal),
        qualleads:parseFloat(data.goals15254) +  parseFloat(data.goals15257) 
      }); 
    } else if (data.segmentGroupName == "Talbott Recovery" || data.segmentGroupName == "IOP: Talbott Recovery Columbus" || data.segmentGroupName == "IOP: Talbott Recovery Dunwoody")  {
      talbottData.push(
        {
          facilityvisits:parseFloat(data.landCountTotal),
          allcalls:parseFloat(data.callCountTotal), 
          callcount:parseFloat(data.callGoalCountTotal), 
          facilityname:data.segmentGroupName, 
          qualified:parseFloat(data.goal11909), 
          greaterthanfour:parseFloat(data.goals15251), 
          greaterthanten:parseFloat(data.goals15254), 
          greaterthanfifteen:parseFloat(data.goals15257), 
          callgoals:parseFloat(data.goalCountTotal),
          qualleads:parseFloat(data.goals15254) +  parseFloat(data.goals15257) 
        }); 
      } else if (data.segmentGroupName == "Skywood Recovery" || data.segmentGroupName == "IOP Foundations Chicago" || data.segmentGroupName == "IOP: Foundations Detroit")  {
        skywoodData.push(
          {
            facilityvisits:parseFloat(data.landCountTotal),
            allcalls:parseFloat(data.callCountTotal), 
            callcount:parseFloat(data.callGoalCountTotal), 
            facilityname:data.segmentGroupName, 
            qualified:parseFloat(data.goal11909), 
            greaterthanfour:parseFloat(data.goals15251), 
            greaterthanten:parseFloat(data.goals15254), 
            greaterthanfifteen:parseFloat(data.goals15257), 
            callgoals:parseFloat(data.goalCountTotal),
            qualleads:parseFloat(data.goals15254) +  parseFloat(data.goals15257) 
          }); 
        }
        else if (data.segmentGroupName == "Michael\'s House" || data.segmentGroupName == "IOP: Michael\'s House Outpatient" || data.segmentGroupName == "IOP: Foundations San Francisco" || data.segmentGroupName == "IOP: The Canyon at Santa Monica" || data.segmentGroupName == "IOP: The Canyon at Encino")  {
          michaelsData.push(
            {
              facilityvisits:parseFloat(data.landCountTotal),
              allcalls:parseFloat(data.callCountTotal), 
              callcount:parseFloat(data.callGoalCountTotal), 
              facilityname:data.segmentGroupName, 
              qualified:parseFloat(data.goal11909), 
              greaterthanfour:parseFloat(data.goals15251), 
              greaterthanten:parseFloat(data.goals15254), 
              greaterthanfifteen:parseFloat(data.goals15257), 
              callgoals:parseFloat(data.goalCountTotal),
              qualleads:parseFloat(data.goals15254) +  parseFloat(data.goals15257) 
            }); 
          } else if (data.segmentGroupName == "Foundations Recovery Network"){
            finalData.push(
              {
                facilityvisits:parseFloat(data.landCountTotal),
                allcalls:parseFloat(data.callCountTotal), 
                callcount:parseFloat(data.callGoalCountTotal), 
                facilityname:data.segmentGroupName, 
                qualified:parseFloat(data.goal11909), 
                greaterthanfour:parseFloat(data.goals15251), 
                greaterthanten:parseFloat(data.goals15254), 
                greaterthanfifteen:parseFloat(data.goals15257), 
                callgoals:parseFloat(data.goalCountTotal),
                qualleads:parseFloat(data.goals15254) +  parseFloat(data.goals15257) 
              }); 
          }
  })

  var val = blackBearData.reduce((accumulator, currentValue) => {
    return {
      facilityvisits: accumulator.facilityvisits + currentValue.facilityvisits,
      allcalls: accumulator.allcalls + currentValue.allcalls,
      callcount: accumulator.callcount + currentValue.callcount,
      facilityname: "Black Bear (Combined)",
      qualified: accumulator.qualified + currentValue.qualified, 
      greaterthanfour:accumulator.greaterthanfour + currentValue.greaterthanfour,
      greaterthanten:accumulator.greaterthanten + currentValue.greaterthanten,
      greaterthanfifteen:accumulator.greaterthanfifteen + currentValue.greaterthanfifteen, 
      callgoals:accumulator.callgoals + currentValue.callgoals,
      qualleads:accumulator.qualleads + currentValue.qualleads
    }
  });

  var valtwo = talbottData.reduce((accumulator, currentValue) => {
    return {
      facilityvisits: accumulator.facilityvisits + currentValue.facilityvisits,
      allcalls: accumulator.allcalls + currentValue.allcalls,
      callcount: accumulator.callcount + currentValue.callcount,
      facilityname: "Talbott (Combined)",
      qualified: accumulator.qualified + currentValue.qualified, 
      greaterthanfour:accumulator.greaterthanfour + currentValue.greaterthanfour,
      greaterthanten:accumulator.greaterthanten + currentValue.greaterthanten,
      greaterthanfifteen:accumulator.greaterthanfifteen + currentValue.greaterthanfifteen, 
      callgoals:accumulator.callgoals + currentValue.callgoals,
      qualleads:accumulator.qualleads + currentValue.qualleads
    }
  });

  var valthree = skywoodData.reduce((accumulator, currentValue) => {
    return {
      facilityvisits: accumulator.facilityvisits + currentValue.facilityvisits,
      allcalls: accumulator.allcalls + currentValue.allcalls,
      callcount: accumulator.callcount + currentValue.callcount,
      facilityname: "Skywood (Combined)",
      qualified: accumulator.qualified + currentValue.qualified, 
      greaterthanfour:accumulator.greaterthanfour + currentValue.greaterthanfour,
      greaterthanten:accumulator.greaterthanten + currentValue.greaterthanten,
      greaterthanfifteen:accumulator.greaterthanfifteen + currentValue.greaterthanfifteen, 
      callgoals:accumulator.callgoals + currentValue.callgoals,
      qualleads:accumulator.qualleads + currentValue.qualleads
    }
  });

  var valfour = michaelsData.reduce((accumulator, currentValue) => {
    return {
      facilityvisits: accumulator.facilityvisits + currentValue.facilityvisits,
      allcalls: accumulator.allcalls + currentValue.allcalls,
      callcount: accumulator.callcount + currentValue.callcount,
      facilityname: "Michael's House (Combined)",
      qualified: accumulator.qualified + currentValue.qualified, 
      greaterthanfour:accumulator.greaterthanfour + currentValue.greaterthanfour,
      greaterthanten:accumulator.greaterthanten + currentValue.greaterthanten,
      greaterthanfifteen:accumulator.greaterthanfifteen + currentValue.greaterthanfifteen, 
      callgoals:accumulator.callgoals + currentValue.callgoals,
      qualleads:accumulator.qualleads + currentValue.qualleads
    }
  });

  finalData.unshift(val, valtwo, valthree, valfour);


  return finalData;  
}

// Format data returned from Infinity for Geckoboard Call Goals from 28 days ago, structure must match the dataset created with lastMonthDatasetPut().
const formatAllMonthData = (data) => {
  let initData = [];

  data.forEach(function(data) {
      if (data.metricMonthName == "January") {
        initData.push(
        {
          month: '2020-01-01',
          calltotal:parseFloat(data.callGoalCountTotal)
        });
      } else if (data.metricMonthName == "February") {
        initData.push(
          {
            month: '2020-02-01',
            calltotal:parseFloat(data.callGoalCountTotal)
          });
      } else if (data.metricMonthName == "March") {
        initData.push(
          {
            month: '2020-03-01',
            calltotal:parseFloat(data.callGoalCountTotal)
          });
      } else if (data.metricMonthName == "April") {
        initData.push(
          {
            month: '2020-04-01',
            calltotal:parseFloat(data.callGoalCountTotal)
          });
      } else if (data.metricMonthName == "May") {
        initData.push(
          {
            month: '2020-05-01',
            calltotal:parseFloat(data.callGoalCountTotal)
          });
      } else if (data.metricMonthName == "June") {
        initData.push(
          {
            month: '2020-06-01',
            calltotal:parseFloat(data.callGoalCountTotal)
          });
      } else if (data.metricMonthName == "July") {
        initData.push(
          {
            month: '2020-07-01',
            calltotal:parseFloat(data.callGoalCountTotal)
          });
      } else if (data.metricMonthName == "August") {
        initData.push(
          {
            month: '2020-08-01',
            calltotal:parseFloat(data.callGoalCountTotal)
          });
      } else if (data.metricMonthName == "September") {
        initData.push(
          {
            month: '2020-09-01',
            calltotal:parseFloat(data.callGoalCountTotal)
          });
      } else if (data.metricMonthName == "October") {
        initData.push(
          {
            month: '2020-010-01',
            calltotal:parseFloat(data.callGoalCountTotal)
          });
      } else if (data.metricMonthName == "December") {
        initData.push(
          {
            month: '2020-11-01',
            calltotal:parseFloat(data.callGoalCountTotal)
          });
      } 
    });

  return initData;  
}




//PUTS Data into created Datasets for Geckoboard
const datasetPut = async (data) => {
  var gb = require("geckoboard")(inputData.apiKey);

  gb.datasets.findOrCreate(
    {
      id: "calls.call_goals_per_day",
      "fields": {
        allcalls: {
          "type": "number",
          "name": "All Calls"
        },
        callcount: {
          "type": "number",
          "name": "Qual. Calls"
        },
        channel: {
          "type": "string",
          "name": "Channel"
        },
        qualified: {
          "type": "number",
          "name": "> 75 sec"
        },
        greaterthanfour: {
          "type":"number",
          "name":"> 4 min"
        },
        greaterthanten: {
          "type":"number",
          "name":"> 10 min"
        },
        greaterthanfifteen: {
          "type":"number",
          "name":"> 15 min"
        },
        callgoals: {
          "type":"number",
          "name":"Goals Per Channel"
        },
        qualleads: {
          "type":"number",
          "name":"Qual. Leads"
        }
      }
    },
    function(err, dataset) {
      if (err) {
        console.error(err);
        return;
      }
  
      dataset.put(
          data,
        function(err) {
          if (err) {
            console.error(err);
            return;
          }
  
          console.log("Today's Dataset created and data added");
        }
      );
    }
  );
}

const lastWeekDatasetPut = async (data) => {
  var gb = require("geckoboard")(inputData.apiKey);

  gb.datasets.findOrCreate(
    {
      id: "calls.calls_compared_last_week",
      "fields": {
        "x_axis": {
          "type": "date"
        },
        callstoday: {
          "type": "number",
          "name": "Qual. calls since midnight"
        },
        callslastweek: {
          "type": "number",
          "name": "Last week qual. calls"
        }
      }
    },
    function(err, dataset) {
      if (err) {
        console.error(err);
        return;
      }
  
      dataset.post(
          data, {unique_by: "x_axis"},
        function(err) {
          if (err) {
            console.error(err);
            return;
          }
  
          console.log("Last Week Dataset created and data added");
        }
      );
    }
  );
}

const lastMonthDatasetPut = async (data) => {
  var gb = require("geckoboard")(inputData.apiKey);

  gb.datasets.findOrCreate(
    {
      id: "calls.call_goals_over_twenty_eight_days",
      "fields": {
        allcalls: {
          "type": "number",
          "name": "All Calls"
        },
        callcount: {
          "type": "number",
          "name": "Qual. Calls"
        },
        channel: {
          "type": "string",
          "name": "Channel"
        },
        qualified: {
          "type": "number",
          "name": "> 75 sec"
        },
        greaterthanfour: {
          "type":"number",
          "name":"> 4 min"
        },
        greaterthanten: {
          "type":"number",
          "name":"> 10 min"
        },
        greaterthanfifteen: {
          "type":"number",
          "name":"> 15 min"
        },
        callgoals: {
          "type":"number",
          "name":"Goals Per Channel"
        },
        qualleads: {
          "type":"number",
          "name":"Qual. Leads"
        },
        avgdayqualcalls: {
          "type":"number",
          "name":"Avg. Qual. Calls"          
        }
      }
    },
    function(err, dataset) {
      if (err) {
        console.error(err);
        return;
      }
  
      dataset.put(
          data,
        function(err) {
          if (err) {
            console.error(err);
            return;
          }
  
          console.log("Today's Dataset created and data added");
        }
      );
    }
  );
}

const facilityDatasetPut = async (data) => {
  var gb = require("geckoboard")(inputData.apiKey);

  gb.datasets.findOrCreate(
    {
      id: "calls.last_month_facility_data",
      "fields": {
        facilityvisits: {
          "type": "number",
          "name": "Total Website Vists"
        },
        allcalls: {
          "type": "number",
          "name": "All Calls"
        },
        callcount: {
          "type": "number",
          "name": "Qual. Calls"
        },
        facilityname: {
          "type": "string",
          "name": "Facility Name"
        },
        qualified: {
          "type": "number",
          "name": "> 75 sec"
        },
        greaterthanfour: {
          "type":"number",
          "name":"> 4 min"
        },
        greaterthanten: {
          "type":"number",
          "name":"> 10 min"
        },
        greaterthanfifteen: {
          "type":"number",
          "name":"> 15 min"
        },
        callgoals: {
          "type":"number",
          "name":"Goals Per Channel"
        },
        qualleads: {
          "type":"number",
          "name":"Qual. Leads"
        }
      }
    },
    function(err, dataset) {
      if (err) {
        console.error(err);
        return;
      }
  
      dataset.put(
          data,
        function(err) {
          if (err) {
            console.error(err);
            return;
          }
  
          console.log("Facility Dataset created and data added");
        }
      );
    }
  );  
}

const AllMonthDatasetPut = async (data) => {
  var gb = require("geckoboard")(inputData.apiKey);

  gb.datasets.findOrCreate(
    {
      id: "calls.calls_per_month",
      "fields": {
        month: {
          "type": "date",
          "name": "Month"
        },
        calltotal: {
          "type": "number",
          "name": "Qual. Calls per Month"
        }
      }
    },
    function(err, dataset) {
      if (err) {
        console.error(err);
        return;
      }
  
      dataset.put(
          data,
        function(err) {
          if (err) {
            console.error(err);
            return;
          }
  
          console.log("Month Dataset created and data added");
        }
      );
    }
  );  
}

//runs the npm run start in terminal, interval is 1 minute
const run = async () => {

  //GET API Call for Channel metrics of current date
  const metricData = await getMetricData();
  //GET API Call for Channel metrics of one week prior (-7 days)
  const lastWeekMetrics = await getLastWeekMetricData();
  //GET API Call for Channel metrics of 28 days prior
  const monthAgoMetrics = await getLastMonthMetricData();
  //GET API Call for Segment Group ID metrics starting 28 days prior to current date
  const facilityMetrics = await getLastMonthFacilityData();
  //GET API Call for Month metrics by current date
  const allMonthMetrics = await getAllMonthsData();

  //Passes metricData() to formatData() for use in datasetPut()
  const chartTodayData = formatData(metricData);
  //Passes metricData() and lastWeekMetrics to formatLastWeekData() for use in lastWeekDatasetPut()
  const chartLastWeekData = formatLastWeekData(metricData, lastWeekMetrics);
  //Passes monthAgoMetrics() to formatMonthAgoData() for use in lastMonthDatasetPut()
  const chartMonthAgoData = formatMonthAgoData(monthAgoMetrics);
  //Passes facilityMetrics() to formatFacilityData() for use in facilityDatasetPut()
  const chartFacilityData = formatFacilityData(facilityMetrics);
  //Passes allMonthMetrics to formatAllMonthData() for use in allMonthDatasetPut()
  const chartAllMonthData = formatAllMonthData(allMonthMetrics);

  console.log('Chart Data: ', chartTodayData);
  console.log('Chart Last Week', chartLastWeekData);
  console.log('Chart Last Month', chartMonthAgoData);
  console.log('Chart Facility Data', chartFacilityData);
  console.log('Chart All Month', chartAllMonthData);

  //Finds dataset calls.call_goals_per_day and puts chartTodayData to Geckoboard
  await datasetPut(chartTodayData);
  //Finds dataset calls.calls_compared_last_week and puts chartLastWeekData to Geckoboard
  await lastWeekDatasetPut(chartLastWeekData);
  //Finds dataset calls.call_goals_over_twenty_eight_days and puts chartMonthAgoData to Geckoboard
  await lastMonthDatasetPut(chartMonthAgoData);
  //Finds dataset calls.last_month_facility_data and puts chartFacilityData to Geckoboard
  await facilityDatasetPut(chartFacilityData);
  //Finds dataset calls.calls_per_month and puts chartAllMonthData to Geckoboard
  await AllMonthDatasetPut(chartAllMonthData);
}

setInterval(run, 60000);

run();